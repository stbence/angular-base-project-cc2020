import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginCredentials } from './login-credentials';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _loginUrl = 'http://localhost:4200/login';

  constructor(private http: HttpClient) { }

  login(user: LoginCredentials): Observable<any> {
    return this.http.post<any>(this._loginUrl, user);
  }
}

